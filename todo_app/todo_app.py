#!/usr/bin/env python

# TODO: Add ability to save to file

shopping_list = []


def show_help():
    """Display the help text"""
    print('Enter HELP for this text, SHOW to the the shopping list content and DONE when you\'re done.')


def show_list():
    count = 1
    for item in shopping_list:
        print('{}: {}'.format(count, item))
        count += 1


def remove_item(idx):
    index = idx - 1
    item = shopping_list.pop(index)
    print('Removed the item: {}'.format(item))


def move_item(idx_from, idx_to):
    index_from = idx_from - 1
    index_to = idx_to - 1

    try:
        shopping_list.insert(index_to, shopping_list.pop(index_from))
        print('Moved the item.')
    except IndexError:
        print('The item or index does not exist. Please try again.')


print('What would you like to shop for?')

while True:
    user_input = raw_input('({} items) >>> '.format(len(shopping_list)))

    if user_input == 'DONE':
        print('Here\'s your list.')
        show_list()
        break
    elif user_input == 'SHOW':
        show_list()
        continue
    elif user_input == 'HELP':
        show_help()
        continue
    elif user_input == 'REMOVE':
        show_list()
        item_index = raw_input('Item number\n(Remove) >>> ')
        remove_item(int(item_index))
        continue
    elif user_input == 'MOVE':
        # Display current list
        show_list()

        # Get the item that will be moved
        idx_from = raw_input('Give me the number of the item you want to move.\n(Move) >>> ')

        # Get index to where we should move it
        idx_to = raw_input('Give me the number to where you want to move it.\n(Move) >>> ')

        move_item(int(idx_from), int(idx_to))
        continue
    else:
        stuff = user_input.split(',')
        if shopping_list:
            index = raw_input('Where would you like to put the items? Insert blank for end of list.\n# ')

            if index:
                try:
                    i = int(index) - 1
                    for item in stuff:
                        shopping_list.insert(i, item.strip())
                        i += 1
                except ValueError:
                    print('The value must be a whole number.')
                    continue
        else:
            for item in stuff:
                shopping_list.append(item.strip())
        continue